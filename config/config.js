

var config = {
	address: "localhost", 	
	port: 8080,
	basePath: "/", 	
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"], 	

	useHttps: false, 		
	httpsPrivateKey: "", 	
	httpsCertificate: "", 	

	language: "en",
	logLevel: ["INFO", "LOG", "WARN", "ERROR"],
	timeFormat: 24,
	units: "metric",


	modules: [
		{
			module: "alert",
		},
		{
			module: "updatenotification",
			position: "top_bar"
		},
		{
			module: "clock",
			position: "top_left"
		},
		{
			module: "calendar",
			header: "Mudit's Calender",
			position: "top_left",
			config: {
				calendars: [
					{
						symbol: "calendar-check",
						url: "https://outlook.office365.com/owa/calendar/88b82313fde14f9abce43eed3700b0dc@bennett.edu.in/3ba053b108c44604ab4a26faa35853064255851970932066323/S-1-8-3489354188-2908762358-2050809257-360750462/reachcalendar.ics"
					}
				]
			}
		},
		{
			module: "compliments",
			position: "lower_third"
		},
		{
			module: "currentweather",
			position: "top_right",
			config: {
				location: "Delhi",
				locationID: "1273294", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				appid: "f3f1a966f3bffb9230f3bf8dc2b99e6d"
			}
		},
		{
			module: "weatherforecast",
			position: "top_right",
			header: "Weather Forecast",
			config: {
				location: "Delhi",
				locationID: "1273294", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				appid: "f3f1a966f3bffb9230f3bf8dc2b99e6d"
			}
		},
		{
			module: "newsfeed",
			position: "bottom_bar",
			config: {
				feeds: [
					{
						title: "Times of India",
						url: "https://timesofindia.indiatimes.com/rssfeeds/-2128936835.cms"
					}
				],
				showSourceTitle: true,
				showPublishDate: true,
				broadcastNewsFeeds: true,
				broadcastNewsUpdates: true
			}
		},
		{


			module: "camera",
			position: 'top_center',
			config: {
				selfieInterval: '3',  // Time interval in seconds before the photo will be taken.
				emailConfig: {
					service: 'Hotmail', // Email provider to use to send email with a photo.
					auth: {
						user: '<name@email.com>', // Your email account
						pass: '<password>'        // Your password for email account
					}
				}

			},
		}
	]
};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") { module.exports = config; }
